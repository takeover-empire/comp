import image1 from './components/img/about.png'
import './App.css';

function App() {
  return (
<>
  <meta charSet="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="App.css" />
  <title>My Website</title>
  {/* Header */}
  <section id="header">
    <div className="header container">
      <div className="nav-bar">
        <div className="brand">
          <a href="#hero">
            <h1>
              <span>Inter</span>N<span>exus</span>
            </h1>
          </a>
        </div>
        <div className="nav-list">
          <div className="hamburger">
            <div className="bar" />
          </div>
          <ul>
            <li>
              <a href="#hero" data-after="Home">
                Home
              </a>
            </li>
            <li>
              <a href="#services" data-after="Service">
                Services
              </a>
            </li>
            <li>
              <a href="#projects" data-after="Projects">
                Projects
              </a>
            </li>
            <li>
              <a href="#about" data-after="About">
                About
              </a>
            </li>
            <li>
              <a href="#contact" data-after="Contact">
                Contact
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  {/* End Header */}
  {/* Hero Section  */}
  <section id="hero">
    <div className="hero container">
      <div>
        <h1>
        From zero experience to your <span />
        </h1>
        <h1>
        Dream job <span />
        </h1>
        <h2>
        Helping students and recent graduates start their career.
        Enabling skills development in a live environment, making students industry ready. <span />
        </h2>
        <a href="#projects" type="button" className="cta">
          Kick Start Your Career
        </a>
      </div>
    </div>
  </section>
  {/* End Hero Section  */}
  {/* Service Section */}
  <section id="services">
    <div className="services container">
      <div className="service-top">
        <h1 className="section-title">
          Serv<span></span>ices
        </h1>
        <p>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum
          deleniti maiores pariatur assumenda quas magni et, doloribus quod
          voluptate quasi molestiae magnam officiis dolorum, dolor provident
          atque molestias voluptatum explicabo!
        </p>
      </div>
      <div className="service-bottom">
        <div className="service-item">
          <div className="icon">
            <img src="https://img.icons8.com/bubbles/100/000000/services.png" />
          </div>
          <h2>Web Design</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis
            debitis rerum, magni voluptatem sed architecto placeat beatae
            tenetur officia quod
          </p>
        </div>
        <div className="service-item">
          <div className="icon">
            <img src="https://img.icons8.com/bubbles/100/000000/services.png" />
          </div>
          <h2>Web Design</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis
            debitis rerum, magni voluptatem sed architecto placeat beatae
            tenetur officia quod
          </p>
        </div>
        <div className="service-item">
          <div className="icon">
            <img src="https://img.icons8.com/bubbles/100/000000/services.png" />
          </div>
          <h2>Web Design</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis
            debitis rerum, magni voluptatem sed architecto placeat beatae
            tenetur officia quod
          </p>
        </div>
        <div className="service-item">
          <div className="icon">
            <img src="https://img.icons8.com/bubbles/100/000000/services.png" />
          </div>
          <h2>Web Design</h2>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis
            debitis rerum, magni voluptatem sed architecto placeat beatae
            tenetur officia quod
          </p>
        </div>
      </div>
    </div>
  </section>
  {/* End Service Section */}
  {/* Projects Section */}
  <section id="projects">
    <div className="projects container">
      <div className="projects-header">
        <h1 className="section-title">
          Recent <span>Projects</span>
        </h1>
      </div>
      <div className="all-projects">
        <div className="project-item">
          <div className="project-info">
            <h1>Project 1</h1>
            <h2>Coding is Love</h2>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad,
              iusto cupiditate voluptatum impedit unde rem ipsa distinctio illum
              quae mollitia ut, accusantium eius odio ducimus illo neque atque
              libero non sunt harum? Ipsum repellat animi, fugit architecto
              voluptatum odit et!
            </p>
          </div>
          <div className="project-img">
            <img src="./img/img-1.png" alt="img" />
          </div>
        </div>
        <div className="project-item">
          <div className="project-info">
            <h1>Project 2</h1>
            <h2>Coding is Love</h2>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad,
              iusto cupiditate voluptatum impedit unde rem ipsa distinctio illum
              quae mollitia ut, accusantium eius odio ducimus illo neque atque
              libero non sunt harum? Ipsum repellat animi, fugit architecto
              voluptatum odit et!
            </p>
          </div>
          <div className="project-img">
            <img src="./img/img-1.png" alt="img" />
          </div>
        </div>
        <div className="project-item">
          <div className="project-info">
            <h1>Project 3</h1>
            <h2>Coding is Love</h2>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad,
              iusto cupiditate voluptatum impedit unde rem ipsa distinctio illum
              quae mollitia ut, accusantium eius odio ducimus illo neque atque
              libero non sunt harum? Ipsum repellat animi, fugit architecto
              voluptatum odit et!
            </p>
          </div>
          <div className="project-img">
            <img src="./img/img-1.png" alt="img" />
          </div>
        </div>
        <div className="project-item">
          <div className="project-info">
            <h1>Project 4</h1>
            <h2>Coding is Love</h2>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad,
              iusto cupiditate voluptatum impedit unde rem ipsa distinctio illum
              quae mollitia ut, accusantium eius odio ducimus illo neque atque
              libero non sunt harum? Ipsum repellat animi, fugit architecto
              voluptatum odit et!
            </p>
          </div>
          <div className="project-img">
            <img src="./img/img-1.png" alt="img" />
          </div>
        </div>
        <div className="project-item">
          <div className="project-info">
            <h1>Project 5</h1>
            <h2>Coding is Love</h2>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad,
              iusto cupiditate voluptatum impedit unde rem ipsa distinctio illum
              quae mollitia ut, accusantium eius odio ducimus illo neque atque
              libero non sunt harum? Ipsum repellat animi, fugit architecto
              voluptatum odit et!
            </p>
          </div>
          <div className="project-img">
            <img src="./img/img-1.png" alt="img" />
          </div>
        </div>
      </div>
    </div>
  </section>
  {/* End Projects Section */}
  {/* About Section */}
  <section id="about">
    <div className="about container">
      <div className="col-left">
        <div className="about-img">
          <img src={image1} alt="img" />
        </div>
      </div>
      <div className="col-right">
        <h1 className="section-title">
          About <span>me</span>
        </h1>
        <h2>Let InternNexus host your webinars.
            Offer seamless support regardless of location or time constraints with the help of remote tools and technologies that enable efficient communication and collaboration.</h2>
        <p>
        InternNexus is a rising platform for connecting students and companies by providing internships to students As well as consulting to the startups or companies for the technology advancements with technical support in the form of resource deployment or remote work.
        </p>
        <a href="#" className="cta">
          Download Resume
        </a>
      </div>
    </div>
  </section>
  {/* End About Section */}
  {/* Contact Section */}
  <section id="contact">
    <div className="contact container">
      <div>
        <h1 className="section-title">
          Contact <span>info</span>
        </h1>
      </div>
      <div className="contact-items">
        <div className="contact-item">
          <div className="icon">
            <img src="https://img.icons8.com/bubbles/100/000000/phone.png" />
          </div>
          <div className="contact-info">
            <h1>Phone</h1>
            <h2>+1 234 123 1234</h2>
            <h2>+1 234 123 1234</h2>
          </div>
        </div>
        <div className="contact-item">
          <div className="icon">
            <img src="https://img.icons8.com/bubbles/100/000000/new-post.png" />
          </div>
          <div className="contact-info">
            <h1>Email</h1>
            <h2>abcd@gmail.com</h2>
            <h2>abcd@gmail.com</h2>
          </div>
        </div>
        <div className="contact-item">
          <div className="icon">
            <img src="https://img.icons8.com/bubbles/100/000000/map-marker.png" />
          </div>
          <div className="contact-info">
            <h1>Address</h1>
            <h2>Vasant Kunj, Delhi- 110085</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  {/* End Contact Section */}
  {/* Footer */}
  <section id="footer">
    <div className="footer container">
      <div className="brand">
        <h1>
          <span>Inter</span>N<span>ex</span>us
        </h1>
      </div>
      <h2>Your Complete Tech Solution</h2>
      <div className="social-icon">
        <div className="social-item">
          <a href="https://www.facebook.com/internnexus/">
            <img src="https://cdn-icons-png.flaticon.com/128/747/747374.png" />
          </a>
        </div>
        <div className="social-item">
          <a href="https://www.instagram.com/internnexusindia/">
            <img src="https://cdn-icons-png.flaticon.com/128/1384/1384031.png" />
          </a>
        </div>
        <div className="social-item">
          <a href="https://twitter.com/i/flow/login?redirect_after_login=%2Finternnexusind">
            <img src="https://cdn-icons-png.flaticon.com/128/1051/1051382.png" />
          </a>
        </div>
        <div className="social-item">
          <a href="https://www.linkedin.com/company/internnexus/">
            <img src="https://cdn-icons-png.flaticon.com/128/1384/1384088.png" />
          </a>
        </div>
      </div>
      <p>Copyright © 2023 InternNexus. All rights reserved</p>
    </div>
  </section>
  {/* End Footer */}
</>

  );
}

export default App;
